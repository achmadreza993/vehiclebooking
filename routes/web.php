<?php

use App\Http\Controllers\KendaraanController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\PegawaiController;
use App\Http\Controllers\PemesananController;
use App\Http\Controllers\PersetujuanController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
	Route::get('/persetujuan', [PersetujuanController::class, 'index'])->name('persetujuan');
	Route::get('/persetujuan/{id}/setuju', [PersetujuanController::class, 'accept'])->name('setuju');
	Route::get('/persetujuan/{id}/tolak', [PersetujuanController::class, 'reject'])->name('tolak');

	Route::get('/', [PemesananController::class, 'index'])->name('pemesanan');
	Route::post('/', [PemesananController::class, 'store'])->name('pemesananSimpan');
	Route::get('/pemesanan/{id}', [PemesananController::class, 'show'])->name('pemesananDetail');
	Route::patch('/pemesanan/{id}/ambil', [PemesananController::class, 'ambil'])->name('pemesananAmbil');
	Route::patch('/pemesanan/{id}/kembali', [PemesananController::class, 'kembali'])->name('pemesananKembali');
	Route::delete('/pemesanan/{id}', [PemesananController::class, 'destroy'])->name('pemesananHapus');

	Route::get('/kendaraan', [KendaraanController::class, 'index'])->name('kendaraan');
	Route::get('/kendaraan/baru', [KendaraanController::class, 'new'])->name('kendaraanBaru');
	Route::post('/kendaraan/store', [KendaraanController::class, 'store'])->name('kendaraanSimpan');
	Route::get('/kendaraan/{id}', [KendaraanController::class, 'show'])->name('kendaraanDetail');
	Route::patch('/kendaraan/{id}', [KendaraanController::class, 'update'])->name('kendaraanGanti');
	Route::delete('/kendaraan/{id}', [KendaraanController::class, 'destroy'])->name('kendaraanHapus');

	Route::get('/pegawai', [PegawaiController::class, 'index'])->name('pegawai');
	Route::get('/pegawai/{id}', [PegawaiController::class, 'show'])->name('pegawaiDetail');

	Route::get('/laporan', function () {
		return redirect('/laporan/Bulan Ini');
	});
	Route::get('/laporan/{periode}', [LaporanController::class, 'index'])->name('laporan');
});
