@extends('layouts.app')

@section('content')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    </div>

    <div class="container-fluid mt--9 pb-6">
        <div class="row mt-5">
            <div class="col-12 mb-5 mb-xl-0 mt-5">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Daftar Kendaraan</h3>
                            </div>
                            <div class="col d-flex justify-content-end">
                                <a href="{{ route('kendaraanBaru') }}" class="btn btn-primary btn-sm" type="button">Tambah
                                    Kendaraan</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" data-toggle="tooltip" title="Nomor Plat">Plat</th>
                                    <th scope="col" data-toggle="tooltip" title="Merk Kendaraan">Merk</th>
                                    <th scope="col" data-toggle="tooltip" title="Tahun Produksi Kendaraan">Tahun</th>
                                    <th scope="col" data-toggle="tooltip" title="Jenis Kendaraan">Jenis</th>
                                    <th scope="col" data-toggle="tooltip" title="Pemilik Kendaraan">Milik</th>
                                    <th scope="col" data-toggle="tooltip" title="Muatan dalam Kg/Org">Muatan</th>
                                    <th scope="col" data-toggle="tooltip" title="Status saat ini">Status</th>
                                    <th scope="col" data-toggle="tooltip" title="Detail">Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($kendaraan as $k)
                                    <tr>
                                        <th scope="row">{{ $k->plat }}</th>
                                        <td>{{ $k->merk }}</td>
                                        <td>{{ $k->tahun }}</td>
                                        <td>{{ $k->jenis }}</td>
                                        <td>{{ $k->milik }}</td>
                                        <td>{{ $k->muatan }} @if ($k->jenis == 'Orang')
                                                orang
                                            @else
                                                Kg
                                            @endif
                                        </td>
                                        <td>{{ $k->status }}</td>
                                        <td><a href="{{ route('kendaraanDetail', ['id' => $k->id]) }}"
                                                class="btn btn-primary btn-sm">Detail</a></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td align="center" colspan="7">Tidak ada kendaraan yang terdaftar</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
