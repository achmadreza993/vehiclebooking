@extends('layouts.app')

@section('content')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>

    <div class="container-fluid mt--9 pb-6">
        <div class="row mt-5">
            <div class="col-12 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Kendaraan Baru</h3>
                            </div>
                        </div>
                    </div>
                    <form class="px-4 pb-4" action="/kendaraan/store" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpPlat">Plat</label>
                                <input type="text" class="form-control" id="inpPlat" name="inpPlat"
                                    placeholder="Nomor Plat">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpMerk">Merk</label>
                                <input type="text" class="form-control" id="inpMerk" name="inpMerk"
                                    placeholder="Nama Merk">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpMilik">Milik</label>
                                <select class="form-control" id="inpMilik" name="inpMilik" onchange="ownerChange()">
                                    <option>Perusahaan</option>
                                    <option>Sewa</option>
                                </select>
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpTahun">Tahun</label>
                                <input type="text" class="form-control" id="inpTahun" name="inpTahun"
                                    placeholder="Tahun dibuat">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpJenis">Jenis</label>
                                <select class="form-control" id="inpJenis" name="inpJenis">
                                    <option>Barang</option>
                                    <option>Orang</option>
                                </select>
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpMuatan">Muatan</label>
                                <input type="number" min="0" class="form-control" id="inpMuatan" name="inpMuatan"
                                    placeholder="0">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpServisKM">Servis KM</label>
                                <input type="number" min="0" class="form-control" id="inpServisKM" name="inpServisKM"
                                    placeholder="0">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpServisBulan">Servis Bulan</label>
                                <input type="number" min="0" class="form-control" id="inpServisBulan"
                                    name="inpServisBulan" placeholder="0">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpServisTerakhir">Servis Terakhir</label>
                                <input type="date" class="form-control" id="inpServisTerakhir" name="inpServisTerakhir">
                            </div>
                        </div>
                        <div id="detailSewa" style="display: none">
                            <h3>Detail Sewa</h3>
                            <div class="row">
                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label for="inpPerusahaan">Perusahaan</label>
                                    <input type="text" class="form-control" id="inpPerusahaan" name="inpPerusahaan"
                                        placeholder="Nama Perusahaan">
                                </div>
                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label for="inpPenanggungJawab">Penanggung Jawab</label>
                                    <input type="text" class="form-control" id="inpPenanggungJawab"
                                        name="inpPenanggungJawab" placeholder="Nama Penanggung Jawab">
                                </div>
                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label for="inpTelp">Telepon</label>
                                    <input type="text" class="form-control" id="inpTelp" name="inpTelp"
                                        placeholder="Telp. Penanggung Jawab">
                                </div>
                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label for="inpTglMulai">TglMulai</label>
                                    <input type="date" class="form-control" id="inpTglMulai" name="inpTglMulai"
                                        placeholder="Tanggal Mulai Sewa">
                                </div>
                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label for="inpTglSelesai">TglSelesai</label>
                                    <input type="date" class="form-control" id="inpTglSelesai" name="inpTglSelesai"
                                        placeholder="Tanggal Selesai Sewa">
                                </div>

                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Tambah Kendaraan</button>
                        <button class="btn btn-secondary" type="reset">Bersihkan</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
    <script>
        const ownerChange = () => {
            if ($('#inpMilik').val() == 'Sewa') {
                $('#detailSewa').removeClass('d-none');
                $('#detailSewa').fadeIn();
            } else {
                $('#detailSewa').fadeOut();
            }
        }
    </script>
@endpush
