@extends('layouts.app')

@section('content')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>

    <div class="container-fluid mt--9 pb-6">
        <div class="row mt-5">
            <div class="col-12 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Detail Kendaraan</h3>
                            </div>
                            <div class="col d-flex justify-content-end">
                                <form action="{{ route('kendaraanHapus', ['id' => $kendaraan->id]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm"
                                        onclick="return confirm('Apakah anda yakin menghapus data kendaraan ini dan data yang berkaitan dengannya?')"
                                        type="submit">Hapus</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <form class="px-4 pb-4" action="{{ route('kendaraanGanti', ['id' => $kendaraan->id]) }}"
                        method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="row">
                            <div class="pb-2 col-12">
                                <h5 class="text-muted m-0">Jadwal Servis</h5>
                                <h4>Pada {{ $kendaraan->jadwalBulan }} atau {{ $kendaraan->jadwalKm }} Km lagi</h4>
                                <h4></h4>
                            </div>
                            <div class="pb-2 col-12">
                                <h5 class="text-muted m-0">Rata-Rata Km/Ltr</h5>
                                <h4>{{ $kendaraan->rataBBM }}</h4>
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpPlat">Plat</label>
                                <input type="text" class="form-control" id="inpPlat" name="inpPlat"
                                    placeholder="Nomor Plat" value="{{ $kendaraan->plat }}">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpMerk">Merk</label>
                                <input type="text" class="form-control" id="inpMerk" name="inpMerk"
                                    placeholder="Nama Merk" value="{{ $kendaraan->merk }}">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpMilik">Milik</label>
                                <input type="text" class="form-control" id="inpMilik" name="inpMilik"
                                    placeholder="Pemilik Kendaraan" value="{{ $kendaraan->milik }}" readonly>
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpTahun">Tahun</label>
                                <input type="text" class="form-control" id="inpTahun" name="inpTahun"
                                    placeholder="Tahun dibuat" value="{{ $kendaraan->tahun }}">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpJenis">Jenis</label>
                                <select class="form-control" id="inpJenis" name="inpJenis">
                                    <option @if ($kendaraan->jenis == 'Barang') selected @endif>Barang</option>
                                    <option @if ($kendaraan->jenis == 'Orang') selected @endif>Orang</option>
                                </select>
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpStatus">Status</label>
                                <select class="form-control" id="inpStatus" name="inpStatus">
                                    <option @if ($kendaraan->status == 'Ada') selected @endif>Ada</option>
                                    <option @if ($kendaraan->status == 'Proses') selected @endif>Proses</option>
                                    <option @if ($kendaraan->status == 'Dipakai') selected @endif>Dipakai</option>
                                    <option @if ($kendaraan->status == 'Diperbaiki') selected @endif>Diperbaiki</option>
                                </select>
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpMuatan">Muatan</label>
                                <input type="number" min="0" class="form-control" id="inpMuatan" name="inpMuatan"
                                    placeholder="0" value="{{ $kendaraan->muatan }}">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpServisKM">Servis KM</label>
                                <input type="number" min="0" class="form-control" id="inpServisKM" name="inpServisKM"
                                    placeholder="0" value="{{ $kendaraan->servisKM }}">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpServisBulan">Servis Bulan</label>
                                <input type="number" min="0" class="form-control" id="inpServisBulan"
                                    name="inpServisBulan" placeholder="0" value="{{ $kendaraan->servisBulan }}">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpServisTerakhir">Servis Terakhir</label>
                                <input type="text" class="form-control" id="inpServisTerakhir" name="inpServisTerakhir"
                                    value="{{ $kendaraan->servisTerakhir }}">
                            </div>
                            <div class="col-12 pb-3">
                                <h3 class="mb-0">Penggunaan Kendaraan</h3>
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpTotalBBM">Total BBM</label>
                                <input type="number" class="form-control" id="inpTotalBBM" name="inpTotalBBM"
                                    placeholder="0" value="{{ $kendaraan->bbm }}">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpRataBBM">Rata-Rata km/Ltr</label>
                                <input type="number" class="form-control" disabled id="inpRataBBM" placeholder="0"
                                    value="{{ $kendaraan->rata }}">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpTotalJarak">Total Jarak Ditempuh</label>
                                <input type="number" class="form-control" id="inpTotalJarak" name="inpTotalJarak"
                                    placeholder="0" value="{{ $kendaraan->jarak }}">
                            </div>
                            <div id="detailSewa" style="display: none" class="px-3 pb-3">
                                <h3>Detail Sewa</h3>
                                <div class="row">
                                    <div class="form-group col-12 col-md-6 col-lg-4">
                                        <label for="inpPerusahaan">Perusahaan</label>
                                        <input type="text" class="form-control" id="inpPerusahaan" name="inpPerusahaan"
                                            placeholder="Nama Perusahaan" value="{{ $kendaraan->perusahaan }}">
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-4">
                                        <label for="inpPenanggungJawab">Penanggung Jawab</label>
                                        <input type="text" class="form-control" id="inpPenanggungJawab"
                                            name="inpPenanggungJawab" placeholder="Nama Penanggung Jawab"
                                            value="{{ $kendaraan->penanggungJawab }}">
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-4">
                                        <label for="inpTelp">Telepon</label>
                                        <input type="text" class="form-control" id="inpTelp" name="inpTelp"
                                            placeholder="Telp. Penanggung Jawab" value="{{ $kendaraan->telp }}">
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-4">
                                        <label for="inpTglMulai">TglMulai</label>
                                        <input type="date" class="form-control" id="inpTglMulai" name="inpTglMulai"
                                            placeholder="Tanggal Mulai Sewa" value="{{ $kendaraan->tglMulai }}">
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-4">
                                        <label for="inpTglSelesai">TglSelesai</label>
                                        <input type="date" class="form-control" id="inpTglSelesai" name="inpTglSelesai"
                                            placeholder="Tanggal Selesai Sewa" value="{{ $kendaraan->tglSelesai }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary"
                            onclick="return confirm('Apakah anda yakin mengubah data kendaraan ini dan data yang berkaitan dengannya?')"
                            type="submit">Ubah Kendaraan</button>
                    </form>
                </div>
            </div>
            <div class="col-12 mb-5 mt-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Daftar Pemakaian</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" data-toggle="tooltip" title="Tanggal kendaraan dipinjam">Tgl. Pinjam
                                    <th scope="col" data-toggle="tooltip" title="Nama Pegawai">Pegawai</th>
                                    </th>
                                    <th scope="col" data-toggle="tooltip" title="Tanggal kendaraan dikembalikan">Waktu
                                        Pengembalian</th>
                                    <th scope="col" data-toggle="tooltip" title="Tempat Tujuan Kendaraan">Tujuan</th>
                                    <th scope="col" data-toggle="tooltip" title="Jarak Tempuh Kendaraan">Jarak</th>
                                    <th scope="col" data-toggle="tooltip" title="BBM yang dikonsumsi">BBM</th>

                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($peminjaman as $p)
                                    <tr>
                                        <td>{{ $p->tglPinjam }}</td>
                                        <td>{{ $p->pegawai }}</td>
                                        <td>{{ $p->waktu }}</td>
                                        <td>{{ $p->tujuan }}</td>
                                        <td>{{ $p->jarak }} Km</td>
                                        <td>{{ $p->bbm }} Ltr</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td align="center" colspan="6">Belum ada peminjaman</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
    <script>
        if ($('#inpMilik').val() == 'Sewa') {
            $('#detailSewa').removeClass('d-none');
            $('#detailSewa').fadeIn();
        } else {
            $('#detailSewa').fadeOut();
        }
    </script>
@endpush
