@extends('layouts.app')

@section('content')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>

    <div class="container-fluid mt--9 pb-6">
        <div class="row mt-5">
            <div class="col-12 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Pemesanan Baru</h3>
                            </div>
                        </div>
                    </div>
                    <form class="px-4 pb-4" action="{{ route('pemesananSimpan') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpNama">Pegawai</label>
                                <select class="form-control" id="inpNama" name="inpNama">
                                    <option>Nama Pegawai</option>
                                    @foreach ($pegawai as $p)
                                        <option>{{ $p->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpTujuan">Tujuan</label>
                                <select class="form-control" id="inpTujuan" name="inpTujuan">
                                    <option>Tujuan</option>
                                    <option>Tambang 1</option>
                                    <option>Tambang 2</option>
                                    <option>Tambang 3</option>
                                    <option>Tambang 4</option>
                                    <option>Tambang 5</option>
                                    <option>Tambang 6</option>
                                </select>
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpKendaraan">Kendaraan</label>
                                <select class="form-control" id="inpKendaraan" name="inpKendaraan">
                                    <option>Kendaraan</option>
                                    @foreach ($kendaraan as $k)
                                        <option>{{ $k->kendaraan }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpTglPinjam" class="form-control-label">Tgl. Pinjam</label>
                                <input class="form-control" type="date" value="" id="inpTglPinjam" name="inpTglPinjam">
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-4">
                                <label for="inpTglKembali" class="form-control-label">Tgl. Kembali</label>
                                <input class="form-control" type="date" value="" id="inpTglKembali" name="inpTglKembali">
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Tambah Pemesanan</button>
                        <button class="btn btn-secondary" type="reset">Bersihkan</button>
                    </form>
                </div>
            </div>
            <div class="col-12 mb-5 mb-xl-0 mt-5">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Daftar Pemesanan</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" data-toggle="tooltip" title="Nama Pegawai">Pegawai</th>
                                    <th scope="col" data-toggle="tooltip" title="Jenis dan plat kendaraan">Kendaraan</th>
                                    <th scope="col" data-toggle="tooltip" title="Tempat Tujuan kendaraan">Tujuan</th>
                                    <th scope="col" data-toggle="tooltip" title="Tanggal kendaraan dipinjam">Tgl. Pinjam
                                    </th>
                                    <th scope="col" data-toggle="tooltip" title="Tanggal kendaraan akan dikembalikan">Tgl.
                                        Kembali</th>
                                    <th scope="col" data-toggle="tooltip" title="Persetujuan Kepala Bagian Penambangan">Per.
                                        Kabag</th>
                                    <th scope="col" data-toggle="tooltip" title="Persetujuan Manajer">Per. Manajer</th>
                                    <th scope="col" data-toggle="tooltip" title="Aksi">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($pemesanan as $pm)
                                    <tr>
                                        <th scope="row">{{ $pm->pegawai }}</th>
                                        <td>{{ $pm->kendaraan }}</td>
                                        <td>{{ $pm->tujuan }}</td>
                                        <td>{{ $pm->tglPinjam }}</td>
                                        <td>{{ $pm->tglKembali }}</td>
                                        <th
                                            class="@if ($pm->persetujuanKPenambangan == 'Ditolak') text-danger @endif @if ($pm->persetujuanKPenambangan == 'Disetujui') text-primary @endif">
                                            {{ $pm->persetujuanKPenambangan }}</th>
                                        <th
                                            class="@if ($pm->persetujuanManajer == 'Ditolak') text-danger @endif @if ($pm->persetujuanManajer == 'Disetujui') text-primary @endif">
                                            {{ $pm->persetujuanManajer }}</th>
                                        <td><a href="{{ route('pemesananDetail', ['id' => $pm->id]) }}"
                                                class="btn btn-primary btn-sm">Detail</a></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td align="center" colspan="6">Belum ada pemesanan</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
