@extends('layouts.app')

@section('content')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>

    <div class="container-fluid mt--9 pb-6">
        <div class="row mt-5">
            <div class="col-12 col-lg mb-5">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Detail Pemesanan</h3>
                            </div>
                            <div class="col d-flex justify-content-end">
                                <form action="{{ route('pemesananHapus', ['id' => $pemesanan->id]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm"
                                        onclick="return confirm('Apakah anda yakin menghapus data pemesanan ini dan data yang berkaitan dengannya?')"
                                        type="submit">Hapus</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row px-4 pb-4">
                        <div class="col-12 col-md-6">
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Nama Pegawai</h5>
                                <h4>{{ $pemesanan->pegawai }}</h4>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Kendaraan</h5>
                                <h4>{{ $pemesanan->kendaraan }}</h4>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Tujuan</h5>
                                <h4>{{ $pemesanan->tujuan }}</h4>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Tanggal Pinjam</h5>
                                <h4>{{ $pemesanan->tglPinjam }}</h4>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Tanggal Kembali</h5>
                                <h4>{{ $pemesanan->tglKembali }}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row px-4 pb-4">
                        <div class="col-12 col-md-6">
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Kepala Bagian Penambangan</h5>
                                <h4>{{ $pemesanan->KPenambangan }}</h4>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Persetujuan Kepala Bagian Penambangan</h5>
                                <h4
                                    class="@if ($pemesanan->persetujuanKPenambangan == 'Ditolak') text-danger @endif @if ($pemesanan->persetujuanKPenambangan == 'Disetujui') text-primary @endif">
                                    {{ $pemesanan->persetujuanKPenambangan }}</h4>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Manajer</h5>
                                <h4>{{ $pemesanan->Manajer }}</h4>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Persetujuan Manajer</h5>
                                <h4
                                    class="@if ($pemesanan->persetujuanManajer == 'Ditolak') text-danger @endif @if ($pemesanan->persetujuanManajer == 'Disetujui') text-primary @endif">
                                    {{ $pemesanan->persetujuanManajer }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if ($pemesanan->persetujuanManajer == 'Disetujui')
                <div class="col-12 col-lg-4 mb-5">
                    @if ($detail == null)
                        <div class="card shadow">
                            <div class="card-header border-0">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h3 class="mb-0">Detail Pengambilan</h3>
                                    </div>
                                </div>
                            </div>
                            <form class="px-4 pb-4" action="{{ route('pemesananAmbil', ['id' => $pemesanan->id]) }}"
                                method="POST">
                                @csrf
                                @method('PATCH')
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label for="inpKMSebelum" class="form-control-label">KM Sebelum Digunakan</label>
                                        <input class="form-control" type="number" min="0" placeholder="0" value=""
                                            id="inpKMSebelum" name="inpKMSebelum">
                                    </div>
                                </div>
                                <button class="btn btn-primary" type="submit">Ganti Detail</button>
                            </form>
                        </div>
                    @else
                        <div class="card shadow">
                            <div class="card-header border-0">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h3 class="mb-0">Detail Pengembalian</h3>
                                    </div>
                                </div>
                            </div>
                            <form class="px-4 pb-4"
                                action="{{ route('pemesananKembali', ['id' => $pemesanan->id]) }}" method="POST">
                                @csrf
                                @method('PATCH')
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label for="inpKMSebelum" class="form-control-label">KM Sebelum Digunakan</label>
                                        <input class="form-control" type="text" id="inpKMSebelum" name="inpKMSebelum"
                                            value="{{ $detail->kmSebelum }}" disabled>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="inpKMSesudah" class="form-control-label">KM Sesudah Digunakan</label>
                                        <input class="form-control" type="number" min="0" placeholder="0"
                                            value="{{ $detail->kmSesudah }}" id="inpKMSesudah" name="inpKMSesudah"
                                            required>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="inpBBM" class="form-control-label">Jumlah BBM yang Digunakan</label>
                                        <input class="form-control" type="number" min="0" placeholder="0"
                                            value="{{ $detail->bbm }}" id="inpBBM" name="inpBBM" required>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="inpWaktu" class="form-control-label">Waktu Pengembalian</label>
                                        <input class="form-control" type="date"
                                            value="{{ $detail->waktuPengembalian }}" id="inpWaktu" name="inpWaktu"
                                            required>
                                    </div>
                                </div>
                                <button class="btn btn-primary" type="submit">Ganti Detail</button>
                            </form>
                        </div>
                    @endif
                </div>
            @endif
        </div>

    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
    <script>
        if ($('#inpMilik').val() == 'Sewa') {
            $('#detailSewa').removeClass('d-none');
            $('#detailSewa').fadeIn();
        } else {
            $('#detailSewa').fadeOut();
        }
    </script>
@endpush
