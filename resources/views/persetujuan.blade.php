<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Argon Dashboard') }}</title>
    <!-- Favicon -->
    <link href="{{ asset('argon') }}/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Extra details for Live View on GitHub Pages -->

    <!-- Icons -->
    <link href="{{ asset('argon') }}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="{{ asset('argon') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <!-- Argon CSS -->
    <link type="text/css" href="{{ asset('argon') }}/css/argon.css?v=1.0.0" rel="stylesheet">
</head>

<body class="{{ $class ?? '' }}">
    @auth()
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        {{-- @include('layouts.navbars.sidebar') --}}
    @endauth

    <div class="main-content">
        @yield('content')

        <!-- Top navbar -->
        <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
            <div class="container-fluid">
                <!-- Brand -->
                <p class="h3 mb-0 text-white text-uppercase px-3 d-inline-block">Persetujuan</p>
                <!-- User -->
                <ul class="navbar-nav align-items-center d-flex">
                    <li class="nav-item dropdown">
                        <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <div class="media align-items-center">
                                <span class="avatar avatar-sm rounded-circle">
                                    <img alt="Image placeholder"
                                        src="{{ asset('argon') }}/img/theme/team-1-800x800.jpg">
                                </span>
                                <div class="media-body ml-2 pr-3 pr-md-0">
                                    <span
                                        class="mb-0 text-sm  font-weight-bold">{{ session('userDetail')->nama }}</span>
                                </div>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                            <div class=" dropdown-header noti-title">
                                <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                            </div>
                            <a href="{{ route('profile.edit') }}" class="dropdown-item">
                                <i class="ni ni-single-02"></i>
                                <span>{{ __('My profile') }}</span>
                            </a>
                            <a href="#" class="dropdown-item">
                                <i class="ni ni-settings-gear-65"></i>
                                <span>{{ __('Settings') }}</span>
                            </a>
                            <a href="#" class="dropdown-item">
                                <i class="ni ni-calendar-grid-58"></i>
                                <span>{{ __('Activity') }}</span>
                            </a>
                            <a href="#" class="dropdown-item">
                                <i class="ni ni-support-16"></i>
                                <span>{{ __('Support') }}</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                <i class="ni ni-user-run"></i>
                                <span>{{ __('Logout') }}</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>

        <div class="container-fluid mt--9 pb-6">
            <div class="row mt-5">
                <div class="col-12 mb-5 mb-xl-0 mt-5">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">Daftar Persetujuan Baru</h3>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col" data-toggle="tooltip" title="Nama Pegawai">Pegawai</th>
                                        <th scope="col" data-toggle="tooltip" title="Jenis dan plat kendaraan">Kendaraan
                                        </th>
                                        <th scope="col" data-toggle="tooltip" title="Tempat tujuan kendaraan">Tujuan
                                        </th>
                                        <th scope="col" data-toggle="tooltip" title="Tanggal kendaraan dipinjam">Tgl.
                                            Pinjam
                                        </th>
                                        <th scope="col" data-toggle="tooltip"
                                            title="Tanggal kendaraan akan dikembalikan">Tgl.
                                            Kembali</th>
                                        <th scope="col" data-toggle="tooltip" title="Aksi" class="text-center">Aksi
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($baru as $b)
                                        <tr>
                                            <th scope="row">{{ $b->pegawai }}</th>
                                            <td>{{ $b->kendaraan }}</td>
                                            <td>{{ $b->tujuan }}</td>
                                            <td>{{ $b->tglPinjam }}</td>
                                            <td>{{ $b->tglKembali }}</td>
                                            <td class="text-center">
                                                <a href="{{ route('setuju', ['id' => $b->id]) }}"
                                                    onclick="return confirm('Apakah anda yakin MENYETUJUI pengajuan ini?')"
                                                    class="btn btn-primary btn-sm">Setuju</a>
                                                <a href="{{ route('tolak', ['id' => $b->id]) }}"
                                                    onclick="return confirm('Apakah anda yakin MENOLAK pengajuan ini?')"
                                                    class="btn btn-danger btn-sm">Tolak</a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td align="center" colspan="6">Belum ada pengajuan baru</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-5 mb-xl-0 mt-5">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">Riwayat Persetujuan</h3>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col" data-toggle="tooltip" title="Nama Pegawai">Pegawai</th>
                                        <th scope="col" data-toggle="tooltip" title="Jenis dan plat kendaraan">Kendaraan
                                        </th>
                                        <th scope="col" data-toggle="tooltip" title="Tempat tujuan kendaraan">Tujuan
                                        </th>
                                        <th scope="col" data-toggle="tooltip" title="Tanggal kendaraan dipinjam">Tgl.
                                            Pinjam
                                        </th>
                                        <th scope="col" data-toggle="tooltip"
                                            title="Tanggal kendaraan akan dikembalikan">Tgl.
                                            Kembali</th>
                                        <th scope="col" data-toggle="tooltip" title="Aksi" class="text-center">
                                            Pengajuan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($riwayat as $r)
                                        <tr>
                                            <th scope="row">{{ $r->pegawai }}</th>
                                            <td>{{ $r->kendaraan }}</td>
                                            <td>{{ $r->tujuan }}</td>
                                            <td>{{ $r->tglPinjam }}</td>
                                            <td>{{ $r->tglKembali }}</td>
                                            @if (session('userDetail')->status == 1)
                                                <th
                                                    class="text-center @if ($r->persetujuanManajer == 'Ditolak') text-danger @endif @if ($r->persetujuanManajer == 'Disetujui') text-primary @endif">
                                                    {{ $r->persetujuanManajer }}
                                                </th>
                                            @endif
                                            @if (session('userDetail')->status == 2)
                                                <th
                                                    class="text-center @if ($r->persetujuanKPenambangan == 'Ditolak') text-danger @endif @if ($r->persetujuanKPenambangan == 'Disetujui') text-primary @endif">
                                                    {{ $r->persetujuanKPenambangan }}
                                                </th>
                                            @endif
                                        </tr>
                                    @empty
                                        <tr>
                                            <td align="center" colspan="6">Tidak ada pengajuan</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src="{{ asset('argon') }}/vendor/jquery/dist/jquery.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    @stack('js')

    <!-- Argon JS -->
    <script src="{{ asset('argon') }}/js/argon.js?v=1.0.0"></script>
</body>

</html>
