@extends('layouts.app')

@section('content')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>

    <div class="container-fluid mt--9 pb-6">
        <div class="row mt-5">
            <div class="col-12 mb-5 mb-xl-0 mt-5">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-12 col-lg-6">
                                <h3 class="mb-0">Daftar Pemesanan</h3>
                            </div>
                            <div class="col-12 col-lg-6 d-flex justify-content-end">
                                <div class="form-group mb-0">
                                    <select class="form-control" id="periode" onchange="changePeriode()">
                                        <option @if (session('periode') == 'Semua') selected @endif>Semua</option>
                                        <option @if (session('periode') == 'Bulan Ini') selected @endif>Bulan Ini</option>
                                        @foreach ($periode as $pr)
                                            <option @if (session('periode') == $pr->periode) selected @endif>{{ $pr->periode }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" data-toggle="tooltip" title="Waktu kendaraan dipinjam(diambil)">Tgl.
                                        Pinjam
                                    </th>
                                    <th scope="col" data-toggle="tooltip" title="Nama Pegawai">Pegawai</th>
                                    <th scope="col" data-toggle="tooltip" title="Jenis dan plat kendaraan">Kendaraan</th>
                                    <th scope="col" data-toggle="tooltip" title="Tempat Tujuan kendaraan">Tujuan</th>
                                    <th scope="col" data-toggle="tooltip" title="Tanggal kendaraan dikembalikan">Tgl.
                                        Kembali</th>
                                    <th scope="col" data-toggle="tooltip" title="Kepala Bagian Penambangan">Kabag</th>
                                    <th scope="col" data-toggle="tooltip" title="Manajer">Manajer</th>
                                    <th scope="col" data-toggle="tooltip" title="KM Kendaraan Sebelum Digunakan">KM Sebelum
                                    </th>
                                    <th scope="col" data-toggle="tooltip" title="KM Kendaraan Setelah Digunakan">KM Sesudah
                                    </th>
                                    <th scope="col" data-toggle="tooltip" title="Total Konsumsi BBM">BBM</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($pemesanan as $pm)
                                    <tr>
                                        <th scope="row">{{ $pm->tglPinjam }}</th>
                                        <td>{{ $pm->pegawai }}</td>
                                        <td>{{ $pm->kendaraan }}</td>
                                        <td>{{ $pm->tujuan }}</td>
                                        <td>{{ $pm->detail->waktuPengembalian }}</td>
                                        <td>{{ $pm->KPenambangan }}</td>
                                        <td>{{ $pm->Manajer }}</td>
                                        <td>{{ $pm->detail->kmSebelum }}</td>
                                        <td>{{ $pm->detail->kmSesudah }}</td>
                                        <td>{{ $pm->detail->bbm }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td align="center" colspan="10">Belum ada pemesanan</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
    <script>
        const changePeriode = () => {
            window.location.replace("/laporan/" + $('#periode').val())
        }
    </script>
@endpush
