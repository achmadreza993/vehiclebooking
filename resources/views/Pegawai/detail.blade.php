@extends('layouts.app')

@section('content')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>

    <div class="container-fluid mt--9 pb-6">
        <div class="row mt-5">
            <div class="col-12 col-md-6 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Detail Pegawai</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row px-4 pb-4">
                        <div class="col-12">
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Nama Pegawai</h5>
                                <h4>{{ $pegawai->nama }}</h4>
                            </div>
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Telepon Pegawai</h5>
                                <h4>{{ $pegawai->telp }}</h4>
                            </div>
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Atasan Pegawai</h5>
                                <h4>{{ $pegawai->atasan }}</h4>
                            </div>
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Posisi Penempatan</h5>
                                <h4>{{ $pegawai->posisi }}</h4>
                            </div>
                            <div class="pb-2">
                                <h5 class="text-muted m-0">Status DiPerusahaan</h5>
                                <h4>{{ $pegawai->status }}</h4>
                            </div>
                            <div class="pb-2">
                                <h5 class="text-muted">Alamat Pegawai</h5>
                                <h4>{{ $pegawai->alamat }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Daftar Peminjaman</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" data-toggle="tooltip" title="Tanggal kendaraan dipinjam">Tgl. Pinjam
                                    <th scope="col" data-toggle="tooltip" title="Jenis dan plat kendaraan">Kendaraan</th>
                                    </th>
                                    <th scope="col" data-toggle="tooltip" title="Tanggal kendaraan dikembalikan">Waktu
                                        Pengembalian</th>
                                    <th scope="col" data-toggle="tooltip" title="Tempat Tujuan Kendaraan">Tujuan</th>
                                    <th scope="col" data-toggle="tooltip" title="Jarak Tempuh Kendaraan">Jarak</th>
                                    <th scope="col" data-toggle="tooltip" title="BBM yang dikonsumsi">BBM</th>

                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($peminjaman as $p)
                                    <tr>
                                        <td>{{ $p->tglPinjam }}</td>
                                        <td>{{ $p->kendaraan }}</td>
                                        <td>{{ $p->waktu }}</td>
                                        <td>{{ $p->tujuan }}</td>
                                        <td>{{ $p->jarak }} Km</td>
                                        <td>{{ $p->bbm }} Ltr</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td align="center" colspan="6">Belum ada peminjaman</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
