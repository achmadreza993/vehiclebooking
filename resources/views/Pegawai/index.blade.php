@extends('layouts.app')

@section('content')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>

    <div class="container-fluid mt--9 pb-6">
        <div class="row mt-5">
            <div class="col-12 mb-5 mb-xl-0 mt-5">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Daftar Pegawai</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" data-toggle="tooltip" title="Nama Pegawai">Nama</th>
                                    <th scope="col" data-toggle="tooltip" title="No. Telp. Pegawai">Telp</th>
                                    <th scope="col" data-toggle="tooltip" title="Atasan Pegawai">Atasan</th>
                                    <th scope="col" data-toggle="tooltip" title="Posisi Penempatan">Posisi</th>
                                    <th scope="col" data-toggle="tooltip" title="Detail">Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($pegawai as $p)
                                    <tr>
                                        <th scope="row">{{ $p->nama }}</th>
                                        <td>{{ $p->telp }}</td>
                                        <td>{{ $p->atasan }}</td>
                                        <td>{{ $p->posisi }}</td>
                                        <td><a href="{{ route('pegawaiDetail', ['id' => $p->id]) }}"
                                                class="btn btn-primary btn-sm">Detail</a></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td align="center" colspan="5">Tidak ada pegawai yang terdaftar</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        {{-- @include('layouts.footers.auth') --}}
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
