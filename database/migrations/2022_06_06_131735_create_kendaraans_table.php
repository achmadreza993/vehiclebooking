<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKendaraansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kendaraans', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('plat');
            $table->string('merk');
            $table->year('tahun');
            $table->enum('jenis', ['Orang', 'Barang']);
            $table->enum('milik', ['Perusahaan', 'Sewa']);
            $table->enum('posisi', ['Pusat', 'Cabang']);
            $table->integer('muatan');
            $table->enum('status', ['Ada', 'Proses', 'Dipakai', 'Diperbaiki']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kendaraans');
    }
}
