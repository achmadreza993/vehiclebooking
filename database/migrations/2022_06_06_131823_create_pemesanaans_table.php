<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatepemesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanans', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('admin');
            $table->string('kendaraan');
            $table->string('pegawai');
            $table->date('tglPinjam');
            $table->date('tglKembali');
            $table->string('KPenambangan');
            $table->boolean('persetujuanKPenambangan');
            $table->string('Manajer');
            $table->boolean('persetujuanManajer');
            $table->string('tujuan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanans');
    }
}
