<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = [
            [
                'id' => 'vAt5tyUU5JG2IAPF',
                'email' => 'admincabang@sixmines.com',
                'email_verified_at' => now(),
                'password' => Hash::make('password'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'zub1zuGRy6Y3sXXu',
                'email' => 'adminpusat@sixmines.com',
                'email_verified_at' => now(),
                'password' => Hash::make('password'),
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        User::insert($admin);

        $Manajer = [
            [
                'id' => 'xT2yjYC3Q45UMa9E',
                'email' => 'manajercabang@sixmines.com',
                'email_verified_at' => now(),
                'password' => Hash::make('password'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'xJoNHhvw1ZcNb2aB',
                'email' => 'manajerpusat@sixmines.com',
                'email_verified_at' => now(),
                'password' => Hash::make('password'),
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        User::insert($Manajer);

        $kepalatambang = [
            [
                'id' => 'eixvDTTuXx22KQIH',
                'email' => 'kepalatambangcabang@sixmines.com',
                'email_verified_at' => now(),
                'password' => Hash::make('password'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'VVKc2YswIKGXc80L',
                'email' => 'kepalatambangpusat@sixmines.com',
                'email_verified_at' => now(),
                'password' => Hash::make('password'),
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        User::insert($kepalatambang);
    }
}
