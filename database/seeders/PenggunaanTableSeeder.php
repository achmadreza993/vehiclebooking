<?php

namespace Database\Seeders;

use App\Models\penggunaan;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PenggunaanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pusat = [
            [
                'id' => '8rTO0bOBwSf9jd8d',
                'bbmTotal' => '0',
                'jarakTotal' => '0',
                'servisKM' => '15000',
                'servisBulan' => '12',
                'servisTerakhir' => '2022-06-07',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'Wb0A2y8wsjzHxeac',
                'bbmTotal' => '0',
                'jarakTotal' => '0',
                'servisKM' => '15000',
                'servisBulan' => '12',
                'servisTerakhir' => '2022-06-07',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'n2anBaC22BcnFg6k',
                'bbmTotal' => '0',
                'jarakTotal' => '0',
                'servisKM' => '20000',
                'servisBulan' => '18',
                'servisTerakhir' => '2022-05-17',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'zmn5uUFWxIHSbwIP',
                'bbmTotal' => '0',
                'jarakTotal' => '0',
                'servisKM' => '10000',
                'servisBulan' => '6',
                'servisTerakhir' => '2022-06-01',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'dr6dCuIHEwNBcHRC',
                'bbmTotal' => '0',
                'jarakTotal' => '0',
                'servisKM' => '10000',
                'servisBulan' => '6',
                'servisTerakhir' => '2022-06-01',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'NeibCI4cg23a5dkS',
                'bbmTotal' => '0',
                'jarakTotal' => '0',
                'servisKM' => '12000',
                'servisBulan' => '9',
                'servisTerakhir' => '2022-05-10',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        penggunaan::insert($pusat);

        $cabang = [
            [
                'id' => '5LPxSJj4BuKGNTlq',
                'bbmTotal' => '0',
                'jarakTotal' => '0',
                'servisKM' => '15000',
                'servisBulan' => '12',
                'servisTerakhir' => '2022-06-01',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'Pp6Hs5H8vdVhLPyu',
                'bbmTotal' => '0',
                'jarakTotal' => '0',
                'servisKM' => '20000',
                'servisBulan' => '16',
                'servisTerakhir' => '2022-05-07',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'EExAIkEoU7yFoQqi',
                'bbmTotal' => '0',
                'jarakTotal' => '0',
                'servisKM' => '10000',
                'servisBulan' => '6',
                'servisTerakhir' => '2022-06-07',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'z5dY0klYhBdL7JVe',
                'bbmTotal' => '0',
                'jarakTotal' => '0',
                'servisKM' => '12000',
                'servisBulan' => '9',
                'servisTerakhir' => '2022-05-07',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        penggunaan::insert($cabang);
    }
}
