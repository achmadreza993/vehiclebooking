<?php

namespace Database\Seeders;

use App\Models\pegawai;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PegawaiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = [
            [
                'id' => 'vAt5tyUU5JG2IAPF',
                'nama' => 'Admin Cabang',
                'alamat' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Culpa sequi illum iusto maiores voluptas quod corrupti impedit quae! Ipsa iusto sit in eveniet nesciunt veritatis blanditiis illo vel harum ea.',
                'telp' => '081234567891',
                'atasan' => '-',
                'status' => '0',
                'posisi' => 'Cabang',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'zub1zuGRy6Y3sXXu',
                'nama' => 'Admin Pusat',
                'alamat' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Culpa sequi illum iusto maiores voluptas quod corrupti impedit quae! Ipsa iusto sit in eveniet nesciunt veritatis blanditiis illo vel harum ea.',
                'telp' => '098765432112',
                'atasan' => '-',
                'status' => '0',
                'posisi' => 'Pusat',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        pegawai::insert($admin);

        $manajer = [
            [
                'id' => 'xT2yjYC3Q45UMa9E',
                'nama' => 'Manajer Cabang',
                'alamat' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Culpa sequi illum iusto maiores voluptas quod corrupti impedit quae! Ipsa iusto sit in eveniet nesciunt veritatis blanditiis illo vel harum ea.',
                'telp' => '081234567891',
                'atasan' => '-',
                'status' => '1',
                'posisi' => 'Cabang',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'xJoNHhvw1ZcNb2aB',
                'nama' => 'Manajer Pusat',
                'alamat' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Culpa sequi illum iusto maiores voluptas quod corrupti impedit quae! Ipsa iusto sit in eveniet nesciunt veritatis blanditiis illo vel harum ea.',
                'telp' => '098765432112',
                'atasan' => '-',
                'status' => '1',
                'posisi' => 'Pusat',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        pegawai::insert($manajer);

        $kabagPenambangan = [
            [
                'id' => 'eixvDTTuXx22KQIH',
                'nama' => 'Kepala Penambangan Cabang',
                'alamat' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Culpa sequi illum iusto maiores voluptas quod corrupti impedit quae! Ipsa iusto sit in eveniet nesciunt veritatis blanditiis illo vel harum ea.',
                'telp' => '081234567891',
                'atasan' => 'xT2yjYC3Q45UMa9E',
                'status' => '2',
                'posisi' => 'Cabang',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'VVKc2YswIKGXc80L',
                'nama' => 'Kepala Penambangan Pusat',
                'alamat' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Culpa sequi illum iusto maiores voluptas quod corrupti impedit quae! Ipsa iusto sit in eveniet nesciunt veritatis blanditiis illo vel harum ea.',
                'telp' => '098765432112',
                'atasan' => 'xJoNHhvw1ZcNb2aB',
                'status' => '2',
                'posisi' => 'Pusat',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        pegawai::insert($kabagPenambangan);

        $pegawai = [
            [
                'id' => 'SZLwUB8lohYjxQP3',
                'nama' => 'Pegawai 1',
                'alamat' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Culpa sequi illum iusto maiores voluptas quod corrupti impedit quae! Ipsa iusto sit in eveniet nesciunt veritatis blanditiis illo vel harum ea.',
                'telp' => '081234567891',
                'atasan' => 'VVKc2YswIKGXc80L',
                'status' => '3',
                'posisi' => 'Pusat',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => '6mFRjVOkW8FYqdOK',
                'nama' => 'Pegawai 2',
                'alamat' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Culpa sequi illum iusto maiores voluptas quod corrupti impedit quae! Ipsa iusto sit in eveniet nesciunt veritatis blanditiis illo vel harum ea.',
                'telp' => '098765432112',
                'atasan' => 'VVKc2YswIKGXc80L',
                'status' => '3',
                'posisi' => 'Pusat',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'zPYKjWhs4h9LwN7F',
                'nama' => 'Pegawai 3',
                'alamat' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Culpa sequi illum iusto maiores voluptas quod corrupti impedit quae! Ipsa iusto sit in eveniet nesciunt veritatis blanditiis illo vel harum ea.',
                'telp' => '098765432112',
                'atasan' => 'VVKc2YswIKGXc80L',
                'status' => '3',
                'posisi' => 'Pusat',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'u5dv3115FXI1Azrx',
                'nama' => 'Pegawai 4',
                'alamat' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Culpa sequi illum iusto maiores voluptas quod corrupti impedit quae! Ipsa iusto sit in eveniet nesciunt veritatis blanditiis illo vel harum ea.',
                'telp' => '098765432112',
                'atasan' => 'eixvDTTuXx22KQIH',
                'status' => '3',
                'posisi' => 'Cabang',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => '16NVoOnE5Hc3UHfJ',
                'nama' => 'Pegawai 5',
                'alamat' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Culpa sequi illum iusto maiores voluptas quod corrupti impedit quae! Ipsa iusto sit in eveniet nesciunt veritatis blanditiis illo vel harum ea.',
                'telp' => '098765432112',
                'atasan' => 'eixvDTTuXx22KQIH',
                'status' => '3',
                'posisi' => 'Cabang',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        pegawai::insert($pegawai);
    }
}
