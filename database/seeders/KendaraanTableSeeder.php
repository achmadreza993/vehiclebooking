<?php

namespace Database\Seeders;

use App\Models\kendaraan;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class KendaraanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pusat = [
            [
                'id' => '8rTO0bOBwSf9jd8d',
                'plat' => 'N 1234 S',
                'merk' => 'Honda',
                'tahun' => '2012',
                'jenis' => 'Orang',
                'milik' => 'Perusahaan',
                'posisi' => 'Pusat',
                'muatan' => '10',
                'status' => 'Ada',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'Wb0A2y8wsjzHxeac',
                'plat' => 'N 2345 S',
                'merk' => 'Honda',
                'tahun' => '2012',
                'jenis' => 'Orang',
                'milik' => 'Perusahaan',
                'posisi' => 'Pusat',
                'muatan' => '12',
                'status' => 'Ada',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'n2anBaC22BcnFg6k',
                'plat' => 'W 3456 S',
                'merk' => 'Honda',
                'tahun' => '2016',
                'jenis' => 'Orang',
                'milik' => 'Sewa',
                'posisi' => 'Pusat',
                'muatan' => '16',
                'status' => 'Ada',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'zmn5uUFWxIHSbwIP',
                'plat' => 'N 4567 S',
                'merk' => 'Volvo',
                'tahun' => '2018',
                'jenis' => 'Barang',
                'milik' => 'Perusahaan',
                'posisi' => 'Pusat',
                'muatan' => '200',
                'status' => 'Ada',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'dr6dCuIHEwNBcHRC',
                'plat' => 'N 5678 S',
                'merk' => 'Volvo',
                'tahun' => '2018',
                'jenis' => 'Barang',
                'milik' => 'Perusahaan',
                'posisi' => 'Pusat',
                'muatan' => '200',
                'status' => 'Ada',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'NeibCI4cg23a5dkS',
                'plat' => 'W 6789 S',
                'merk' => 'Volvo',
                'tahun' => '2020',
                'jenis' => 'Barang',
                'milik' => 'Sewa',
                'posisi' => 'Pusat',
                'muatan' => '250',
                'status' => 'Ada',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        kendaraan::insert($pusat);

        $cabang = [
            [
                'id' => '5LPxSJj4BuKGNTlq',
                'plat' => 'AG 7890 S',
                'merk' => 'Honda',
                'tahun' => '2012',
                'jenis' => 'Orang',
                'milik' => 'Perusahaan',
                'posisi' => 'Cabang',
                'muatan' => '10',
                'status' => 'Ada',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'Pp6Hs5H8vdVhLPyu',
                'plat' => 'W 8901 S',
                'merk' => 'Honda',
                'tahun' => '2016',
                'jenis' => 'Orang',
                'milik' => 'Sewa',
                'posisi' => 'Cabang',
                'muatan' => '16',
                'status' => 'Ada',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'EExAIkEoU7yFoQqi',
                'plat' => 'N 9012 S',
                'merk' => 'Volvo',
                'tahun' => '2018',
                'jenis' => 'Barang',
                'milik' => 'Perusahaan',
                'posisi' => 'Cabang',
                'muatan' => '200',
                'status' => 'Ada',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'z5dY0klYhBdL7JVe',
                'plat' => 'W 2901 S',
                'merk' => 'Volvo',
                'tahun' => '2020',
                'jenis' => 'Barang',
                'milik' => 'Sewa',
                'posisi' => 'Cabang',
                'muatan' => '250',
                'status' => 'Ada',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        kendaraan::insert($cabang);
    }
}
