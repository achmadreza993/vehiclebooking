<?php

namespace Database\Seeders;

use App\Models\sewa;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SewaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pusat = [
            [
                'id' => 'n2anBaC22BcnFg6k',
                'Perusahaan' => 'PT. Sewa Pickup',
                'penanggungJawab' => 'Achmad',
                'tglMulai' => '2022-04-01',
                'tglSelesai' => '2023-04-01',
                'telp' => '098765432112',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'NeibCI4cg23a5dkS',
                'Perusahaan' => 'PT. Sewa Truck',
                'penanggungJawab' => 'Achmad',
                'tglMulai' => '2022-03-01',
                'tglSelesai' => '2023-03-01',
                'telp' => '098765432112',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        sewa::insert($pusat);

        $cabang = [
            [
                'id' => 'Pp6Hs5H8vdVhLPyu',
                'Perusahaan' => 'PT. Sewa Pickup',
                'penanggungJawab' => 'Reza',
                'tglMulai' => '2022-04-02',
                'tglSelesai' => '2023-04-02',
                'telp' => '089765432123',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 'z5dY0klYhBdL7JVe',
                'Perusahaan' => 'PT. Sewa Truck',
                'penanggungJawab' => 'Reza',
                'tglMulai' => '2022-03-02',
                'tglSelesai' => '2023-03-02',
                'telp' => '089765432123',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        sewa::insert($cabang);
    }
}
