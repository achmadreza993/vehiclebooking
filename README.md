## Daftar User
### Admin 
<ul>
    <li>adminpusat@sixmines.com</li>
    <li>admincabang@sixmines.com </li>
</ul>

### Manajer
<ul>
    <li>manajerpusat@sixmines.com</li>
    <li>manajercabang@sixmines.com</li>
</ul>

### Kepala Bagian Tambang
<ul>
    <li>kepalatambangpusat@sixmines.com</li>
    <li>kepalatambangcabang@sixmines.com</li>
</ul>

## Env Detail
Database Version : 10.4.22
PHP Version : 7.3.7
framework : 8.83.15

## Panduan Penggunaan
### Pemesanan - Tambah Baru
<ol>
    <li>Buka Halaman Pemesanan dengan menekan tombol pemesanan di samping kiri website.</li>
    <li>Isi data sesuai dengan formulir.</li>
    <li>Tekan tombol 'Tambah Pemesanan' untuk mengirimkan data yang sudah diisi.</li>
    <li>Data sudah tersimpan dan bisa dilihat dibagian bawah formulir.</li>
</ol>

### Pemesanan - Hapus
<ol>
    <li>Buka Halaman Pemesanan dengan menekan tombol pemesanan di samping kiri website.</li>
    <li>Geser kebawah untuk melihat daftar pesanan lalu klik tombol Detail.</li>
    <li>Setelah halaman detail terbuka, klik tombol hapus yang berada di bagian kanan atas kelompok Detail Pemesanan.</li>
    <li>Setelah itu akan muncul tampilan konfirmasi, anda bisa menekan OK untuk melanjutkan menghapus dan Cancel untuk membatalkannya.</li>
    <li>Data sudah terhapus.</li>
</ol>

