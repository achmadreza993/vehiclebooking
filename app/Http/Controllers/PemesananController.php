<?php

namespace App\Http\Controllers;

use App\Models\detail;
use App\Models\kendaraan;
use App\Models\pegawai;
use App\Models\pemesanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PemesananController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        session(['page' => 'Pemesanan']);
        $p = pegawai::where('posisi', session('userDetail')->posisi)->where('status', 3)->orderBy('nama')->get('nama');
        $k = kendaraan::where('posisi', session('userDetail')->posisi)->where('status', 'Ada')->selectRaw('Concat(merk, " ", plat," ", jenis) as kendaraan')->orderBy('kendaraan')->get();
        $pemesanan = pemesanan::where('admin', session('userDetail')->id)->get();
        foreach ($pemesanan as $pm) {
            $pm->pegawai = pegawai::where('id', $pm->pegawai)->first()->nama;
            $pm->KPenambangan = pegawai::where('id', $pm->KPenambangan)->first()->nama;
            $pm->Manajer = pegawai::where('id', $pm->Manajer)->first()->nama;
            if ($pm->persetujuanKPenambangan == 0) {
                $pm->persetujuanKPenambangan = 'Diajukan';
                $pm->persetujuanManajer = 'Belum Diajukan';
            }
            if ($pm->persetujuanKPenambangan == 1) {
                $pm->persetujuanKPenambangan = 'Ditolak';
                $pm->persetujuanManajer = 'Tidak Diajukan';
            }
            if ($pm->persetujuanKPenambangan == 2) {
                $pm->persetujuanKPenambangan = 'Disetujui';
                if ($pm->persetujuanManajer == 0) $pm->persetujuanManajer = 'Diajukan';
                if ($pm->persetujuanManajer == 1) $pm->persetujuanManajer = 'Ditolak';
                if ($pm->persetujuanManajer == 2) $pm->persetujuanManajer = 'Disetujui';
            }
        }
        return view('Pemesanan.pemesanan', ['pegawai' => $p, 'kendaraan' => $k, 'pemesanan' => $pemesanan]);
    }

    public function store(Request $request)
    {
        $pegawai = pegawai::where('nama', $request->inpNama)->first();
        $kpenambangan = pegawai::where('id', $pegawai->atasan)->first();
        $manajer = pegawai::where('id', $kpenambangan->atasan)->first();
        $inp = [
            'id' => $this->randString(),
            'admin' => session('userDetail')->id,
            'kendaraan' => $request->inpKendaraan,
            'pegawai' => $pegawai->id,
            'tglPinjam' => $request->inpTglPinjam,
            'tglKembali' => $request->inpTglKembali,
            'tujuan' => $request->inpTujuan,
            'KPenambangan' => $kpenambangan->id,
            'persetujuanKPenambangan' => false,
            'Manajer' => $manajer->id,
            'persetujuanManajer' => false,
            'created_at' => now(),
            'updated_at' => now(),
        ];
        $kendaraan = kendaraan::selectRaw("*,concat(merk, ' ', plat, ' ', jenis) as 'detail'")->having('detail', 'LIKE', $request->inpKendaraan)->first();
        $kendaraan->status = 'Proses';
        $kendaraan->update();
        pemesanan::insert($inp);
        return redirect('/');
    }

    public function show(pemesanan $id)
    {
        $id->pegawai = pegawai::where('id', $id->pegawai)->first()->nama;
        $id->KPenambangan = pegawai::where('id', $id->KPenambangan)->first()->nama;
        $id->Manajer = pegawai::where('id', $id->Manajer)->first()->nama;
        if ($id->persetujuanKPenambangan == 0) {
            $id->persetujuanKPenambangan = 'Diajukan';
            $id->persetujuanManajer = 'Belum Diajukan';
        }
        if ($id->persetujuanKPenambangan == 1) {
            $id->persetujuanKPenambangan = 'Ditolak';
            $id->persetujuanManajer = 'Tidak Diajukan';
        }
        if ($id->persetujuanKPenambangan == 2) {
            $id->persetujuanKPenambangan = 'Disetujui';
            if ($id->persetujuanManajer == 0) $id->persetujuanManajer = 'Diajukan';
            if ($id->persetujuanManajer == 1) $id->persetujuanManajer = 'Ditolak';
            if ($id->persetujuanManajer == 2) $id->persetujuanManajer = 'Disetujui';
        }
        $detail = detail::where('id', $id->id)->first();
        return view('Pemesanan.detail', ['pemesanan' => $id, 'detail' => $detail]);
    }

    public function ambil(Request $request, $id)
    {

        $kendaraan = kendaraan::selectRaw("*,concat(merk, ' ', plat, ' ', jenis) as 'detail'")->having('detail', 'LIKE', pemesanan::where('id', $id)->first()->kendaraan)->first();
        $kendaraan->status = 'Dipakai';
        $kendaraan->update();
        detail::insert([
            'id' => $id,
            'kmSebelum' => $request->inpKMSebelum,
            'created_at' => now(),
        ]);
        return redirect('/');
    }

    public function kembali(Request $request, detail $id)
    {
        $id->kmSesudah = $request->inpKMSesudah;
        $id->bbm = $request->inpBBM;
        $id->waktuPengembalian = $request->inpWaktu;
        $id->update();
        return redirect('/');
    }

    public function destroy(pemesanan $id)
    {
        detail::where('id', $id->id)->delete();
        $id->delete();
        return redirect('/');
    }
}
