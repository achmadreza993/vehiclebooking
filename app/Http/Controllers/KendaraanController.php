<?php

namespace App\Http\Controllers;

use App\Models\detail;
use App\Models\kendaraan;
use App\Models\pegawai;
use App\Models\pemesanan;
use App\Models\penggunaan;
use App\Models\sewa;
use Illuminate\Http\Request;

class KendaraanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        session(['page' => 'Kendaraan']);
        return view('Kendaraan.index', ['kendaraan' => kendaraan::where('posisi', session('userDetail')->posisi)->get()]);
    }

    public function new()
    {
        session(['page' => 'Kendaraan Baru']);
        return view('Kendaraan.new');
    }

    public function show(kendaraan $id)
    {
        $p = penggunaan::where('id', $id->id)->first();
        $id->bbm = $p->bbmTotal;
        $id->jarak = $p->jarakTotal;
        $id->rata = 0;
        if ($id->jarak != 0 & $id->bbm != 0) $id->rata = $id->jarak / $id->bbm;
        $id->servisKM = $p->servisKM;
        $id->servisBulan = $p->servisBulan;
        $id->servisTerakhir = $p->servisTerakhir;
        $id->rataBBM = 0;
        if ($p->jarakTotal != 0 && $p->bbmTotal != 0) $id->rataBBM = $p->jarakTotal / $p->bbmTotal;
        $id->jadwalKm = $p->servisKM - ($p->jarakTotal % $p->servisKM);
        $id->jadwalBulan = date('Y-m-d',  strtotime($p->servisTerakhir) + ($p->servisBulan * 30 * 24 * 60 * 60));
        if ($id->milik == 'Sewa') {
            $s = sewa::where('id', $id->id)->first();
            $id->perusahaan = $s->perusahaan;
            $id->penanggungJawab = $s->penanggungJawab;
            $id->telp = $s->telp;
            $id->tglMulai = $s->tglMulai;
            $id->tglSelesai = $s->tglSelesai;
        } else {
            $id->perusahaan = '';
            $id->penanggungJawab = '';
            $id->telp = '';
            $id->tglMulai = '';
            $id->tglSelesai = '';
        }
        session(['page' => 'Detail Kendaraan']);

        $pemesanan = pemesanan::where('kendaraan', $id->merk . ' ' . $id->plat . ' ' . $id->jenis)->where('persetujuanKPenambangan', 2)->where('persetujuanManajer', 2)->get();
        foreach ($pemesanan as $p) {
            $d = detail::where('id', $p->id)->first();
            $p->pegawai = pegawai::where('id', $p->pegawai)->first()->nama;
            $p->jarak = $d->kmSesudah - $d->kmSebelum;
            $p->waktu = $d->waktuPengembalian;
            $p->bbm = $d->bbm;
        }

        return view('Kendaraan.detail', ['kendaraan' => $id, 'peminjaman' => $pemesanan]);
    }

    public function store(Request $request)
    {
        $id = $this->randString();
        $k = [
            'id' => $id,
            'plat' => $request->inpPlat,
            'merk' => $request->inpMerk,
            'tahun' => $request->inpTahun,
            'jenis' => $request->inpJenis,
            'milik' => $request->inpMilik,
            'posisi' => session('userDetail')->posisi,
            'muatan' => $request->inpMuatan,
            'status' => 'Ada',
            'created_at' => now(),
            'updated_at' => now()
        ];
        kendaraan::insert($k);
        $p = [
            'id' => $id,
            'bbmTotal' => 0,
            'jarakTotal' => 0,
            'servisKM' => $request->inpServisKM,
            'servisBulan' => $request->inpServisBulan,
            'servisTerakhir' => $request->inpServisTerakhir,
            'created_at' => now(),
            'updated_at' => now()
        ];
        penggunaan::insert($p);
        if ($request->inpMilik == 'Sewa') {
            $s = [
                'id' => $id,
                'perusahaan' => $request->inpPerusahaan,
                'penanggungJawab' => $request->inpPenanggungJawab,
                'telp' => $request->inpTelp,
                'tglMulai' => $request->inpTglMulai,
                'tglSelesai' => $request->inpTglSelesai,
                'created_at' => now(),
                'updated_at' => now()
            ];
            sewa::insert($s);
        }
        session(['page' => 'Kendaraan Baru']);
        return view('Kendaraan.new');
    }

    public function update(Request $request, $id)
    {
        $k = [
            'plat' => $request->inpPlat,
            'merk' => $request->inpMerk,
            'tahun' => $request->inpTahun,
            'jenis' => $request->inpJenis,
            'posisi' => session('userDetail')->posisi,
            'muatan' => $request->inpMuatan,
            'status' => 'Ada',
            'updated_at' => now()
        ];
        kendaraan::where('id', $id)->update($k);
        $p = [
            'bbmTotal' => $request->inpTotalBBM,
            'jarakTotal' => $request->inpTotalJarak,
            'servisKM' =>   $request->inpServisKM,
            'servisBulan' => $request->inpServisBulan,
            'servisTerakhir' => $request->inpServisTerakhir,
            'updated_at' => now()
        ];
        penggunaan::where('id', $id)->update($p);
        if ($request->inpMilik == 'Sewa') {
            $s = [
                'perusahaan' => $request->inpPerusahaan,
                'penanggungJawab' => $request->inpPenanggungJawab,
                'telp' => $request->inpTelp,
                'tglMulai' => $request->inpTglMulai,
                'tglSelesai' => $request->inpTglSelesai,
                'updated_at' => now()
            ];
            sewa::where('id', $id)->update($s);
        }
        return redirect('/kendaraan/' . $id);
    }

    public function destroy($id)
    {
        $kendaraan = kendaraan::where('id', $id)->first();
        $pemesanan = pemesanan::where('kendaraan', $kendaraan->merk . ' ' . $kendaraan->plat . ' ' . $kendaraan->jenis)->get();
        foreach ($pemesanan as $pm) {
            detail::where('id', $pm->id)->delete();
            $pm->delete();
        }
        kendaraan::where('id', $id)->delete();
        penggunaan::where('id', $id)->delete();
        sewa::where('id', $id)->delete();
        return redirect('/kendaraan');
    }
}
