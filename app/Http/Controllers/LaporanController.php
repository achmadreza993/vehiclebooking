<?php

namespace App\Http\Controllers;

use App\Models\detail;
use App\Models\kendaraan;
use App\Models\pegawai;
use App\Models\pemesanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LaporanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index($per)
    {
        session(['page' => 'Laporan']);
        session(['periode' => $per]);
        $periode = pemesanan::where('admin', session('userDetail')->id)->selectRaw("distinct(DATE_FORMAT(tglPinjam, '%M %Y')) AS 'periode'")->orderBy('periode', 'ASC')->get();
        $pemesanan = null;
        if ($per == 'Semua') $pemesanan = pemesanan::where('admin', session('userDetail')->id)->orderBy('tglPinjam')->get();
        else if ($per == 'Bulan Ini') $pemesanan = pemesanan::where('admin', session('userDetail')->id)->whereRaw("tglPinjam LIKE '%" . now()->format('Y-m') . "%'")->orderBy('tglPinjam')->get();
        else $pemesanan = pemesanan::where('admin', session('userDetail')->id)->whereRaw("DATE_FORMAT(tglPinjam, '%M %Y') LIKE '%" . $per . "%'")->orderBy('tglPinjam')->get();

        foreach ($pemesanan as $pm) {
            $pm->pegawai = pegawai::where('id', $pm->pegawai)->first()->nama;
            $pm->KPenambangan = pegawai::where('id', $pm->KPenambangan)->first()->nama;
            $pm->Manajer = pegawai::where('id', $pm->Manajer)->first()->nama;
            $pm->detail = detail::where('id', $pm->id)->first();
        }
        return view('Laporan.index', ['pemesanan' => $pemesanan, 'periode' => $periode]);
    }
}
