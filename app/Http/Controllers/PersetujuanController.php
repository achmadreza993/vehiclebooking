<?php

namespace App\Http\Controllers;

use App\Models\kendaraan;
use App\Models\pegawai;
use App\Models\pemesanan;

class PersetujuanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('persetujuan');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        session(['page' => 'Persetujuan']);
        if (session('userDetail')->status == 1) $baru = pemesanan::where('Manajer', session('userDetail')->id)->where('persetujuanManajer', 0)->get();
        if (session('userDetail')->status == 2) $baru = pemesanan::where('KPenambangan', session('userDetail')->id)->where('persetujuanKPenambangan', 0)->get();
        foreach ($baru as $b) {
            $b->pegawai = pegawai::where('id', $b->pegawai)->first()->nama;
        }
        if (session('userDetail')->status == 1) $riwayat = pemesanan::where('Manajer', session('userDetail')->id)->where('persetujuanManajer', '>', 0)->get();
        if (session('userDetail')->status == 2) $riwayat = pemesanan::where('KPenambangan', session('userDetail')->id)->where('persetujuanKPenambangan', '>', 0)->get();
        foreach ($riwayat as $r) {
            $r->pegawai = pegawai::where('id', $r->pegawai)->first()->nama;
            if (session('userDetail')->status == 1) {
                if ($r->persetujuanManajer == 1) $r->persetujuanManajer = 'Ditolak';
                if ($r->persetujuanManajer == 2) $r->persetujuanManajer = 'Disetujui';
            }
            if (session('userDetail')->status == 2) {
                if ($r->persetujuanKPenambangan == 1) $r->persetujuanKPenambangan = 'Ditolak';
                if ($r->persetujuanKPenambangan == 2) $r->persetujuanKPenambangan = 'Disetujui';
            }
        }
        return view('persetujuan', ['baru' => $baru, 'riwayat' => $riwayat]);
    }

    public function accept(pemesanan $id)
    {
        if (session('userDetail')->status == 1) $id->persetujuanManajer = 2;
        if (session('userDetail')->status == 2) $id->persetujuanKPenambangan = 2;
        $id->update();
        return redirect('/persetujuan');
    }

    public function reject(pemesanan $id)
    {
        $kendaraan = kendaraan::selectRaw("*,concat(merk, ' ', plat, ' ', jenis) as 'detail'")->having('detail', 'LIKE', $id->kendaraan)->first();
        $kendaraan->status = 'Ada';
        $kendaraan->update();
        if (session('userDetail')->status == 1) $id->persetujuanManajer = 1;
        if (session('userDetail')->status == 2) $id->persetujuanKPenambangan = 1;
        $id->update();
        return redirect('/persetujuan');
    }
}
