<?php

namespace App\Http\Controllers;

use App\Models\detail;
use App\Models\kendaraan;
use App\Models\pegawai;
use App\Models\pemesanan;

class PegawaiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $pegawai = pegawai::where('posisi', session('userDetail')->posisi)->where('status', 3)->get();
        foreach ($pegawai as $p) {
            $p->atasan = pegawai::where('id', $p->atasan)->first()->nama;
            if ($p->status == '1') $p->status = 'Manajer';
            if ($p->status == '2') $p->status = 'Kep. Bag. Penambangan';
            if ($p->status == '3') $p->status = 'Pegawai';
        }
        session(['page' => 'Pegawai']);
        return view('Pegawai.index', ['pegawai' => $pegawai]);
    }

    public function show(pegawai $id)
    {
        if ($id->atasan != '-') $id->atasan = pegawai::where('id', $id->atasan)->first()->nama;
        if ($id->status == '1') $id->status = 'Manajer';
        if ($id->status == '2') $id->status = 'Kep. Bag. Penambangan';
        if ($id->status == '3') $id->status = 'Pegawai';
        session(['page' => 'Detail Pegawai']);
        $pemesanan = pemesanan::where('pegawai', $id->id)->where('persetujuanKPenambangan', 2)->where('persetujuanManajer', 2)->get();
        foreach ($pemesanan as $p) {
            $d = detail::where('id', $p->id)->first();
            $p->jarak = $d->kmSesudah - $d->kmSebelum;
            $p->waktu = $d->waktuPengembalian;
            $p->bbm = $d->bbm;
        }
        return view('Pegawai.detail', ['pegawai' => $id, 'peminjaman' => $pemesanan]);
    }
}
