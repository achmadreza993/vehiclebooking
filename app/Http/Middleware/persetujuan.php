<?php

namespace App\Http\Middleware;

use App\Models\pegawai;
use Closure;
use Illuminate\Http\Request;

class persetujuan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (session('userDetail') == null) session(['userDetail' => pegawai::where('id', auth()->id())->first()]);

        if (session('userDetail')->status == 0) {
            return redirect('/');
        }
        return $next($request);
    }
}
